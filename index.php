<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="styles.css">
    <title></title>
  </head>
  <body>
    <header class="--bold">
      <div class="container">
        <nav class="navigation main__navigation">
          <a href="#">Accueil</a>
          <a href="#">Collections</a>
          <a href="#">Equipe</a>
          <a href="#">Nous</a>
        </nav>
        <nav class="navigation log__navigation">
          <a href="#">S'inscrire</a>
          <a href="#">Se connecter</a>
        </nav>
      </div>

    </header>
    <main class="container main__index">
      <section class="subscribe__to__newsletter">
        <h1>S'abonner à la newsletter</h1>
        <form class="" action="./Back/Actions/addSubscriber.php" method="post">
          <input class="input" type="text" name="firstname" value="" placeholder="Prénom" maxlength="50" required>
          <input class="input" type="text" name="lastname" value="" placeholder="Nom" maxlength="50" required>
          <fieldset>
            <label>Quel est votre genre ?</label><br>
            <input type="radio" name="gender" value="Homme" checked><label>Homme</label>
            <input type="radio" name="gender" value="Femme"><label>Femme</label>
            <input type="radio" name="gender" value="Autre"><label>Autre</label>
          </fieldset>
          <input class="input" type="email" name="email" pattern="(.*)[@](.*)[.](\w*)" title="Entrez une addresse email valide" value="" placeholder="Email" maxlength="100" required>
          <input class="input" type="date" max="<?php echo strval(date("Y")-18)."-".strval(date("m"))."-".strval(date("d")); ?>" name="birth" value="" required>
          <input class="input" type="text" pattern="((\+|00)\d{2}|022|0)(\s|-|)(\d{2})(\s|-|)(\d{3})(\s|-|)(\d{2})(\s|-|)(\d{2})" title="Entrez un numéro de téléphone valide" name="phone" value="" placeholder="Numéro de téléphone" maxlength="12" required>
          <input class="input" type="text" name="country" value="" placeholder="Pays" maxlength="100" required>
          <div class="input__question">
            <label for="question">Vous avez une question ? Posez-la nous !</label>
            <textarea name="question" rows="8" minlength="15" required></textarea>
          </div>

          <input class="button" type="submit" name="submit" value="S'abonner">
        </form>
      </section>
    </main>
  </body>
</html>
