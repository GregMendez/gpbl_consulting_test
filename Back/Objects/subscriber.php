<?php

require '../Actions/connectDb.php';



class Subscriber
{

  private $minDaysToWait = 1;

  public function verifySubscriber($actualIP, $firstname, $lastname, $gender, $email, $birth, $phone, $country, $creatAt) {

    if(count($this->getSubscriberByIP($actualIP)) > 0)
    {
      $subscriber = $this->getSubscriberByIP($actualIP);

      $updateAt = date('Y-m-d');

      // Vérifi si le champs "updateAt" est null
      if($subscriber[0]['updateAt'] !== null) {
        $diff = abs(strtotime($updateAt) - strtotime($subscriber[0]['updateAt']));
      }
      else {
        $diff = abs(strtotime($updateAt) - strtotime($subscriber[0]['createAt']));
      }

      // Récupération des différences entre les dates
      $years  = floor($diff / (365 * 60 * 60 * 24));
      $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
      $days   = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 *24) / (60 * 60 * 24));

      // SI DIFFERENCE ENTRE CREATEAT && NOW >= 1j alors
      if($days >= $this->minDaysToWait){
        return $this->updateSubscriber($subscriber[0]['idSubscriber'], $firstname, $lastname, $gender, $email, $birth, $phone, $country, $updateAt, $subscriber[0]['counter'] + 1);
      }

    }
    else
    {
      return $this->addSubscriber($firstname, $lastname, $gender, $email, $birth, $phone, $country, $actualIP, $creatAt);
    }
  }


  public function getSubscribers()
  {
    $db = dbConnection();

    $response = $db->prepare(
        'SELECT * FROM subscriber',
        [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]
    );

    $response->execute();

    return $response->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getSubscriberByIP($ip)
  {
    $db = dbConnection();

    $response = $db->prepare(
        'SELECT * FROM subscriber WHERE ip = :ip',
        [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]
    );

    $response->execute([
      'ip' => $ip,
    ]);


    return $response->fetchAll(PDO::FETCH_ASSOC);
  }



  public function addSubscriber($firstname, $lastname, $gender, $email, $birth, $phone, $country, $ip, $createAt)
  {
    $db = dbConnection();

    $response = $db->prepare(
        'INSERT INTO subscriber SET firstname = :firstname, lastname = :lastname, type = :gender, email = :email, birth = :birth, phone = :phone, country = :country, ip = :ip, createAt = :createAt, counter = 1',
        [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]
    );

    $response->execute([
      'firstname' => $firstname,
      'lastname' => $lastname,
      'gender' => $gender,
      'email' => $email,
      'birth' => $birth,
      'phone' => $phone,
      'country' => $country,
      'ip' => $ip,
      'createAt' => $createAt,
    ]);

    return $response;
  }

  public function updateSubscriber($idSubscriber, $firstname, $lastname, $gender, $email, $birth, $phone, $country, $updateAt, $counter)
  {
    $db = dbConnection();

    $response = $db->prepare(
        'UPDATE subscriber SET firstname = :firstname, lastname = :lastname, type = :gender, email = :email, birth = :birth, phone = :phone, country = :country, updateAt = :updateAt, counter = :counter WHERE idSubscriber = :idSubscriber',
        [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]
    );

    $response->execute([
      'idSubscriber' => $idSubscriber,
      'firstname' => $firstname,
      'lastname' => $lastname,
      'gender' => $gender,
      'email' => $email,
      'birth' => $birth,
      'phone' => $phone,
      'country' => $country,
      'updateAt' => $updateAt,
      'counter' => $counter,
    ]);

    if($response)
    {
      $response = "L'enregistrement à été correctement modifié";
    }

    return $response;
  }
}


?>
