<?php

require '../Objects/subscriber.php';


$minAge = 18;


if(!empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['gender'])
  && !empty($_POST['email']) && !empty($_POST['birth']) && !empty($_POST['phone'])
  && !empty($_POST['country']))
  {
    $subscriber = new Subscriber();


    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $gender = $_POST['gender'];
    $email = $_POST['email'];
    $birth = $_POST['birth'];
    $phone = $_POST['phone'];
    $country = $_POST['country'];

    /************** REGEX EMAIL *******************************/

    /************** REGEX PHONE ********************************/

    /************* -18 NOT ALLOWED *****************************/
    $createAt = date('Y-m-d');
    $diff = $createAt - $birth;

    // Si l'utilisateur est majeur
    if($diff >= $minAge) {

      /**************  récupération IP *********************/
      $ip = $_SERVER['REMOTE_ADDR'];
      $subscriber->verifySubscriber($ip, $firstname, $lastname, $gender, $email, $birth, $phone, $country, $createAt);
      header('Location: ../../index.php');
    }
    else
    {
      header('Location: ../../index.php?dateErr=yes');
    }
  }


?>
