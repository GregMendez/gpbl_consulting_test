1. Veuillez svp importer le script sql (le script comporte la création de la base) et ajouter un utilisateur à cette base de données.

2. Veuillez svp changer le compte utilisateur utilisé pour la connexion à la base de données ainsi que le host si vous ne testez pas en local. Ces informations sont contenues dans le fichier "connectDB.php se trouvant dans "Back/Actions/connectDB.php". Il est nécessaire d'utiliser un utilisateur valide de votre base de données perso.
